/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50637
 Source Host           : localhost:3306
 Source Schema         : yx-ag_auth

 Target Server Type    : MySQL
 Target Server Version : 50637
 File Encoding         : 65001

 Date: 12/03/2021 10:00:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_client
-- ----------------------------
DROP TABLE IF EXISTS `auth_client`;
CREATE TABLE `auth_client`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '服务名',
  `locked` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auth_client
-- ----------------------------
INSERT INTO `auth_client` VALUES ('1', 'smart-gate', '123456', 'smart-gate', '0', '服务网关', NULL, '', '', '', '2017-07-07 21:51:32', '1', '管理员', '0:0:0:0:0:0:0:1', '', '', '', '', '', '', '', '');
INSERT INTO `auth_client` VALUES ('18', 'smart-transaction', '123456', 'smart-transaction', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('20', 'smart-dict', '123566', 'smart-dict', '0', '数据字典服务', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('205dd87d19054195bbb5d00b08623160', 'smart-charge-server', '123456', 'smart-charge-server', '0', '收费模块', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('21', 'smart-demo-depart-data', '123456', 'smart-demo-depart-data', '0', '测试服务', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('22', 'smart-workflow', '123456', 'smart-workflow', '0', '工作流服务', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('24', 'smart-app', '123456', 'smart-app', '0', NULL, NULL, NULL, NULL, NULL, '2018-05-16 20:34:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('2d8db2e05f3c4b1b98ec960389b66de7', 'smart-navigate-server', '123456', 'smart-navigate-server', '0', '导航模块', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('3', 'smart-admin', '123456', 'smart-admin', '0', '', NULL, NULL, NULL, NULL, '2017-07-06 21:42:17', '1', '管理员', '0:0:0:0:0:0:0:1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('6', 'smart-auth', '123456', 'smart-auth', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('7', 'smart-tool', '123456', 'smart-tool', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('74ae69b813164537999bcc518799c948', 'smart-dev-manage', '123456', 'smart-dev-manage', '0', '设备管理', '2019-08-08 15:47:09', NULL, NULL, NULL, '2019-08-08 15:47:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('834bdebed7694286bd761d5c450264af', 'smart-onsite-server', '123456', 'smart-onsite-server', '0', '场内流程', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('8401c522b4374623873533052b61ad75', 'smart-parking-server', '123456', 'smart-parking-server', '0', '城市智能停车平台', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('9d0a85c39d2740c18b06f9d6edb610df', 'smart-pay-server', '123456', 'smart-pay-server', '0', '支付模块', '2019-08-14 15:11:42', NULL, NULL, NULL, '2019-08-14 15:11:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('aabfa5a27c784cb7a157058d4ad9cb32', 'smart-mqtt-common', '123456', 'smart-mqtt-common', '0', 'mqtt服务', '2018-08-14 09:55:18', NULL, NULL, NULL, '2018-08-14 09:55:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('bdcef8079e11406ea6781bb2bd6a36b9', 'smart-generator', '123456', 'smart-generator', '0', '代码生成器', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('cce9b8e5ed8446e1a039c3c8ae95a1ac', 'smart-exper-app', '123456', 'smart-exper-app', '0', '物联网体验馆APP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('d8c0b438498f4e5f80e4570c44a7ca45', 'smart-roadside-server', '123456', 'smart-roadside-server', '0', '路测流程', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client` VALUES ('f1eae53b5dce4537ad8e7891d5db523d', 'yuncitys.com-exper-server', '123456', 'yuncitys.com-exper-server', '0', '物联网体验馆', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for auth_client_service
-- ----------------------------
DROP TABLE IF EXISTS `auth_client_service`;
CREATE TABLE `auth_client_service`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `crt_time` datetime(0) NULL DEFAULT NULL,
  `crt_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `crt_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `crt_host` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `attr8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 216 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auth_client_service
-- ----------------------------
INSERT INTO `auth_client_service` VALUES (21, '4', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (43, '3', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (45, '12', '16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (46, '18', '18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (77, '3', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (78, 'f1eae53b5dce4537ad8e7891d5db523d', '21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (91, '3', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (92, 'f1eae53b5dce4537ad8e7891d5db523d', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (93, '8401c522b4374623873533052b61ad75', '22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (164, '3', 'f1eae53b5dce4537ad8e7891d5db523d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (165, 'f1eae53b5dce4537ad8e7891d5db523d', 'f1eae53b5dce4537ad8e7891d5db523d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (166, 'cce9b8e5ed8446e1a039c3c8ae95a1ac', 'f1eae53b5dce4537ad8e7891d5db523d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (167, 'aabfa5a27c784cb7a157058d4ad9cb32', 'f1eae53b5dce4537ad8e7891d5db523d', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (174, '8401c522b4374623873533052b61ad75', '205dd87d19054195bbb5d00b08623160', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (175, '3', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (176, '8401c522b4374623873533052b61ad75', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (177, '6', '24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (185, 'f1eae53b5dce4537ad8e7891d5db523d', 'cce9b8e5ed8446e1a039c3c8ae95a1ac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (186, '6', 'cce9b8e5ed8446e1a039c3c8ae95a1ac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (187, '3', 'cce9b8e5ed8446e1a039c3c8ae95a1ac', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (188, '1', '74ae69b813164537999bcc518799c948', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (189, '6', '74ae69b813164537999bcc518799c948', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (190, '8401c522b4374623873533052b61ad75', '74ae69b813164537999bcc518799c948', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (191, '3', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (192, '6', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (193, '20', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (194, '24', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (195, 'f1eae53b5dce4537ad8e7891d5db523d', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (196, '8401c522b4374623873533052b61ad75', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (197, 'bdcef8079e11406ea6781bb2bd6a36b9', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (198, '205dd87d19054195bbb5d00b08623160', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (199, '2d8db2e05f3c4b1b98ec960389b66de7', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (200, '834bdebed7694286bd761d5c450264af', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (201, 'd8c0b438498f4e5f80e4570c44a7ca45', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (202, 'cce9b8e5ed8446e1a039c3c8ae95a1ac', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (203, '74ae69b813164537999bcc518799c948', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (204, '3', '8401c522b4374623873533052b61ad75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (205, '205dd87d19054195bbb5d00b08623160', '8401c522b4374623873533052b61ad75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (206, '6', '8401c522b4374623873533052b61ad75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (207, '24', '8401c522b4374623873533052b61ad75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (208, '74ae69b813164537999bcc518799c948', '8401c522b4374623873533052b61ad75', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (209, '3', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (210, 'f1eae53b5dce4537ad8e7891d5db523d', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (211, '8401c522b4374623873533052b61ad75', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (212, 'bdcef8079e11406ea6781bb2bd6a36b9', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (213, '205dd87d19054195bbb5d00b08623160', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (214, 'cce9b8e5ed8446e1a039c3c8ae95a1ac', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `auth_client_service` VALUES (215, '74ae69b813164537999bcc518799c948', '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gateway_route
-- ----------------------------
DROP TABLE IF EXISTS `gateway_route`;
CREATE TABLE `gateway_route`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '映射路劲',
  `service_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '映射服务',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '映射外连接',
  `retryable` tinyint(1) NULL DEFAULT NULL COMMENT '是否重试',
  `enabled` tinyint(1) NOT NULL COMMENT '是否启用',
  `strip_prefix` tinyint(1) NULL DEFAULT NULL COMMENT '是否忽略前缀',
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime(0) NULL DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gateway_route
-- ----------------------------
INSERT INTO `gateway_route` VALUES ('admin', '/admin/**', 'smart-admin', NULL, 0, 1, 1, 'Smart', '1', '2018-02-25 14:33:30', 'Smart', '1', '2018-02-25 14:38:31');
INSERT INTO `gateway_route` VALUES ('app', '/app/**', 'smart-app', NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `gateway_route` VALUES ('auth', '/auth/**', 'smart-auth', NULL, 0, 1, 1, NULL, NULL, NULL, 'Smart', '1', '2018-02-25 14:29:51');
INSERT INTO `gateway_route` VALUES ('center', '/center/**', 'smart-center', NULL, 0, 1, 1, 'Smart', '1', '2018-02-26 12:50:51', 'Smart', '1', '2018-02-26 12:50:51');
INSERT INTO `gateway_route` VALUES ('charge', '/charge/**', 'smart-charge-server', NULL, 0, 1, 1, 'Smart', '1', '2018-08-29 19:25:18', 'Smart', '1', '2018-08-29 19:25:18');
INSERT INTO `gateway_route` VALUES ('devManage', '/devManage/**', 'smart-dev-manage', NULL, 0, 1, 1, 'Smart', '1', '2019-08-08 16:02:45', 'Smart', '1', '2019-08-08 16:04:29');
INSERT INTO `gateway_route` VALUES ('dict', '/dict/**', 'smart-dict', NULL, 0, 1, 1, NULL, NULL, NULL, 'Smart', '1', '2018-02-25 14:41:07');
INSERT INTO `gateway_route` VALUES ('experapp', '/experapp/**', 'smart-exper-app', NULL, 0, 1, 1, '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-08-01 11:58:04', '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-08-01 11:58:23');
INSERT INTO `gateway_route` VALUES ('iotexper', '/iotexper/**', 'yuncitys.com-exper-server', NULL, 0, 1, 1, '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-07-19 15:47:53', '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-07-19 15:48:06');
INSERT INTO `gateway_route` VALUES ('mqtt', '/mqtt/**', 'smart-mqtt-common', NULL, 0, 1, 1, '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-08-14 10:40:39', '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-08-14 10:40:51');
INSERT INTO `gateway_route` VALUES ('parking', '/parking/**', 'smart-parking-server', NULL, 0, 1, 1, 'Smart', '1', '2018-07-23 13:53:24', '云创智城', 'b0500adf48014f4a8edc26dc534a2ecf', '2018-07-23 13:53:51');
INSERT INTO `gateway_route` VALUES ('pay', '/pay/**', 'smart-pay-server', NULL, 0, 1, 1, 'Smart', '1', '2019-08-14 15:12:17', 'Smart', '1', '2019-08-14 15:12:17');
INSERT INTO `gateway_route` VALUES ('tool', '/tool/**', 'smart-tool', NULL, 0, 1, 1, NULL, NULL, '2018-04-02 21:04:47', NULL, NULL, '2018-04-02 21:04:52');
INSERT INTO `gateway_route` VALUES ('workflow', '/wf/**', 'smart-workflow', NULL, 0, 1, 1, NULL, NULL, '2018-04-05 13:58:08', NULL, NULL, '2018-04-05 13:58:14');

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details`  (
  `client_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource_ids` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `client_secret` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `scope` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorized_grant_types` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authorities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_token_validity` int(11) NULL DEFAULT NULL,
  `refresh_token_validity` int(11) NULL DEFAULT NULL,
  `additional_information` varchar(4096) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autoapprove` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `crt_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `crt_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `crt_time` datetime(0) NULL DEFAULT NULL,
  `upd_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `upd_user_id` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `upd_time` datetime(0) NULL DEFAULT NULL,
  `is_deleted` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_disabled` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('app', NULL, 'app', 'read', 'password,refresh_token', 'http://localhost:7999/#/', NULL, 14400, 14400, NULL, NULL, NULL, 'Smart', '1', '2019-04-02 15:47:47', 'Smart', '1', '2019-04-02 15:48:08', NULL, NULL);
INSERT INTO `oauth_client_details` VALUES ('client', '', 'client', 'read', 'password,refresh_token,authorization_code', 'http://localhost:4040/sso/login', NULL, 7200, 2592000, '{}', 'true', NULL, NULL, NULL, NULL, 'Smart', '1', '2018-08-31 11:29:01', NULL, NULL);
INSERT INTO `oauth_client_details` VALUES ('experapp', NULL, 'experapp', 'read', 'password,refresh_token', 'http://localhost:7998/#/', NULL, 14400, 2592000, NULL, 'true', NULL, 'Smart', '1', '2018-08-14 19:05:37', 'Smart', '1', '2018-08-14 19:06:43', NULL, NULL);
INSERT INTO `oauth_client_details` VALUES ('vue', NULL, 'vue', 'read', 'password,refresh_token', 'http://localhost:9527/#/', NULL, 14400, 2592000, '{}', 'true', '', NULL, NULL, NULL, 'Smart', '1', '2018-07-31 10:42:11', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
